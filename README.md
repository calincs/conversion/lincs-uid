# LINCS UID generator

This is a web service that generates unique IDs with a length of 11 characters. Based on the [NanoID](https://github.com/ai/nanoid/) library.

## Building and running

 * Make sure that Node.js, NPM, Postgres and Docker are installed on your local machine
 * Clone the repo
 * To start the service, run `docker-compose -f docker-compose.yml up`

## Usage

Sending a GET request to http://localhost:5000/generateUID or http://localhost:5000//generateMultipleUID  or returns a json packet with a uid field.

## LINCS deployment

The service is available at https://uid.lincsproject.ca/generateUID and https://uid.lincsproject.ca/generateMultipleUID

Sample result /generateUID:

```json
{"uid":"NKp3Mi3e7Ep"}
```

Sample result /generateMultipleUID?num=3 (NOTE: if no amount is passed, the default is 5 uids):

```json
{"uids":["5p3pAJd9h5I","a53aKLhw0sF","mjvVCsVOXcN"]}
```
