'use strict';

const express = require("express");
const app = express();
const { Client } = require("pg");

const { customAlphabet } = require('nanoid');
const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
const nanoid = customAlphabet(alphabet, 11);

const portNum = 5000;
const maxTries = 20;
const client = new Client(process.env.DATABASE_URL);

async function startUp() {
    console.log("Connecting to " + process.env.DATABASE_URL);
    await client.connect();
    await client.query("CREATE TABLE IF NOT EXISTS uids (uid VARCHAR(255) PRIMARY KEY);");

    app.listen(portNum);
    console.log("Running service on port: " + portNum);
}

app.get('/generateUID', async function (req, res) {
    let output = {};

    let check = true;
    let count = 0;
    while (check == true) {
        if (count > maxTries) {
            check = false;
            console.log("Failed to generate UID");
            res.send("Failed to generate UID");
            return;
        }

        output["uid"] = nanoid();
        let uidCheck = await client.query("SELECT EXISTS (SELECT * FROM uids WHERE uid = '" + output["uid"] + "' LIMIT 1);");
        if (uidCheck["rows"][0]["exists"] == false) {
            check = false;
        } else {
            console.log("Generated pre-existing uid");
            count = count + 1;
        }
    }

    await client.query("INSERT INTO uids (uid) VALUES('" + output["uid"] + "');");

    res.send(output);
});

app.get('/generateMultipleUID', async function (req, res) {
    let numUid = req.query.num;
    if (numUid == null) {
        numUid = 5;
    }

    let output = {};
    output["uids"] = [];

    let check = true;
    let count = 0;
    while (check == true) {
        if (count > maxTries) {
            check = false;
            console.log("Failed to generate UID");
            res.send("Failed to generate UID");
            return;
        }

        let uid = nanoid();
        let uidCheck = await client.query("SELECT EXISTS (SELECT * FROM uids WHERE uid = '" + uid + "' LIMIT 1);");
        if (uidCheck["rows"][0]["exists"] == false) {
            output["uids"].push(uid);
            count = 0;
            if (output["uids"].length >= numUid) {
                check = false;
            }
        } else {
            console.log("Generated pre-existing uid");
            count = count + 1;
        }
    }

    let i, j, temp, chunk = 10;
    for (i = 0,j = output["uids"].length; i < j; i += chunk) {
        temp = output["uids"].slice(i, i + chunk);
        await client.query("INSERT INTO uids (uid) VALUES('" + temp.join(", ") + "');");
    }

    res.send(output);
});

app.get('/', function (req, res) {
    res.send('<html>The LINCS UID generator is available at <a href="./generateUID">/generateUID</a> and <a href="./generateMultipleUID">/generateMultipleUID</a></html>');
});

startUp();
